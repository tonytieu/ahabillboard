﻿using AhhaBllboard.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AhhaBllboard.Core.Service
{
    public interface IFoodService
    {
        Task<Food[]> GetAll();
    }
}
