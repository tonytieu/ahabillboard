﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AhhaBllboard.Core.Model
{
    public class Food
    {
        public Guid FoodId { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public string VideoUrl { get; set; }

        public virtual ICollection<FoodImage> FoodImages { get; set; }
    }

    public class FoodImage
    {
        public Guid FoodImageId { get; set; }
        public string Url { get; set; }

    }
}
