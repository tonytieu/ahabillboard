﻿/*jslint unparam: true */
//'use strict';

// Change this to the location of your server-side upload handler:
var url = window.location.hostname === 'blueimp.github.io' ?
            '//jquery-file-upload.appspot.com/' : '/Files/UploadFileImage';

var uploadButton = $('<button/>')
    .addClass('btn btn-primary btn-sm btn-upload')
    .prop('disabled', true)
    .text('Processing...')
    .on('click', function (e) {
        e.stopPropagation();
        e.preventDefault();
        var $this = $(this),
            data = $this.data();
        $this
            .off('click')
            .text('Abort')
            .on('click', function () {
                $this.remove();
                data.abort();
            });
        data.submit().always(function () {
            $this.remove();
           
        });
    });

var uploadCustomButton = $('<button/>')
    .addClass('btn btn-primary btn-sm btn-upload btn-img-upload')
    .prop('disabled', true)
    .text('Processing...')
    .on('click', function (e) {
        e.stopPropagation();
        e.preventDefault();
        var $this = $(this),
            data = $this.data();

        if (!$this.next().hasClass("btn-img-abort"))
        {
            var abort = $this.clone(true);
            abort.text("Abort").removeClass("btn-img-upload").addClass("btn-img-abort")
                .on('click', function () {
                    if ($this.text == 'Abort') {
                        $this.remove();
                        data.abort();
                    }
                });
            $this.after(abort);
        } 
        
        data.submit().always(function () {
            $this.next().remove();
            $this.remove();
        });;
    });

var removeButton = $('<a href="#"/>')
    .addClass('btn btn-danger btn-sm btn-remove')
    .text('Remove')
    .on('click', function (e) {
        e.stopPropagation();
        var $this = $(this),
            data = $this.data();
        

        //console.log(data.deletedPath);

        // callbak if defined
        if (typeof fileUploadImageRemove != "undefined") {
            fileUploadImageRemove(data, e);
        }

        $this.parents('.file-container').remove();
    });

var FileType = {
    IMAGE: /(\.|\/)(gif|jpe?g|png)$/i,
    DOCUMENT: /(\.|\/)(docx?|pptx?|xlsx?|pdf)$/i
}

function initializeFileUpload(parentContainer, fileType, fileUploadDoneCallback, isMultipleDownloads) {
    console.log(parentContainer);
    $(parentContainer + ' ' + '#fileupload').fileupload({
        url: url,
        dataType: 'json',
        autoUpload: false,
        acceptFileTypes: fileType,
        maxFileSize: 999000,
        // Enable image resizing, except for Android and Opera,
        // which actually support image resizing, but fail to
        // send Blob objects via XHR requests:
        disableImageResize: /Android(?!.*Chrome)|Opera/
            .test(window.navigator.userAgent),
        previewMaxWidth: 100,
        previewMaxHeight: 100,
        previewCrop: true
    }).on('fileuploadadd', function (e, data) {
        // if single upload
        if (!isMultipleDownloads) {
            $(parentContainer + ' ' + "#files").html("");
        }

        data.context = $('<div/>').addClass('file-container').appendTo(parentContainer + ' ' + '#files');
        
        $.each(data.files, function (index, file) {
            var node = $('<p/>')
                    .append($('<span/>').text(file.name));
            if (!index) {
                node
                    .append('<br>')
                    .append(uploadButton.clone(true).data(data));
                node
                    .append(removeButton.clone(true).data(data));
            }
            node.appendTo(data.context);
        });
    }).on('fileuploadprocessalways', function (e, data) {
        var index = data.index,
            file = data.files[index],
            node = $(data.context.children()[index]);
        if (file.preview) {
            node
                .prepend('<br>')
                .prepend(file.preview);
        }
        if (file.error) {
            node
                .append('<br>')
                .append($('<span class="text-danger"/>').text(file.error));
        }
        if (index + 1 === data.files.length) {
            data.context.find('button')
                .text('Upload')
                .prop('disabled', !!data.files.error);
        }
    }).on('fileuploadprogressall', function (e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
        $(parentContainer + ' ' + '#progress .progress-bar').css(
            'width',
            progress + '%'
        );
        $(parentContainer).find(".btn-remove").addClass('disabled');
        }).on('fileuploaddone', function (e, data) {
            console.log("fileuploaddone 1");
            console.log(e);
            console.log(data);
            console.log("fileuploaddone 2");
        $.each(data.result.files, function (index, file) {
            if (file.url) {
                var link = $('<a>')
                    .attr('target', '_blank')
                    .attr('class', 'img-link')
                    .prop('href', file.url);

                //console.log($(data.context));
                $(data.context.children()[index]).wrap(link);
                data.deletedPath = file.path;
                data.fullUrl = file.url;
                //$(data.context).append(removeButton.clone(true).data(data));
                $(parentContainer).find(".btn-remove").removeClass('disabled');
            } else if (file.error) {
                var error = $('<span class="text-danger"/>').text(file.error);
                $(data.context.children()[index])
                    .append('<br>')
                    .append(error);
            }
        });
        
        // callbak if defined
        if (typeof fileUploadDoneCallback != "undefined" && fileUploadDoneCallback) {
            fileUploadDoneCallback(data);
        }
    }).on('fileuploadfail', function (e, data) {
        $.each(data.files, function (index) {
            var error = $('<span class="text-danger"/>').text('File upload failed.');
            $(data.context.children()[index])
                .append('<br>')
                .append(error);
        });
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
}

function getFileUploadUrls(parentContainer) {
    return $(parentContainer + " #files a").map(function (d) {
        return $(this).attr("href");
    });
}
