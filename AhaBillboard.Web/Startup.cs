﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AhaBillboard.Web.Startup))]
namespace AhaBillboard.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
