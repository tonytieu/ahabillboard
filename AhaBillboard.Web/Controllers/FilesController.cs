﻿
using AdvanceLane.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace AhaBillboard.Web.Controllers
{

    public class FilesController : Controller
    {
        string relativePath = "/img";
        //private BlobStorageRepo repo = BlobStorageRepo.getInstance();

        public ActionResult RenderPartialFileUpload()
        {
            return View("_PartialFileUpload");
        }

        [HttpPost]
        public async Task<JsonResult> UploadFileImage()
        {
            HttpPostedFileBase upload = Request.Files["UploadedFile"];

            if (!System.IO.Directory.Exists(Server.MapPath("~") + relativePath))
            {
                System.IO.Directory.CreateDirectory(Server.MapPath("~") + relativePath);
            }

            if (upload != null && upload.ContentLength > 0)
            {
                var random = Guid.NewGuid();
                var fileName = random + "_" + upload.FileName;
                System.Diagnostics.Debug.WriteLine("File name " + upload.FileName);

                // filepath on local server
                var filePath = Path.Combine(Server.MapPath(Request.ApplicationPath) + relativePath, fileName);

                //System.Diagnostics.Debug.WriteLine("File Url " + uri);

                // create returned format according to blueimp
                //string uploadBase64 = Utility.HttpPostedFileBaseToBase64(upload);
                List<FileDetailViewModel> lst = new List<FileDetailViewModel>();
                FileDetailViewModel f1 = new FileDetailViewModel();

                f1.url = ResolveLocalFileToUrl(relativePath, fileName);

                //fileName = await repo.UploadImage(fileName, upload.InputStream);
                //f1.name = upload.FileName;
                //f1.url = repo.BlobUrlPrefix + fileName;
                f1.path = "/" + relativePath + "/" + fileName;
                lst.Add(f1);

                // save to Local
                upload.SaveAs(filePath);

                

                return Json(new { success = false, files = lst });
            }
            return Json(new { success = false, message = "Upload failed" });

        }

        [HttpPost]
        public async Task<JsonResult> DeleteFileImage(string filePath)
        {
            //var serverPath = Path.Combine(Server.MapPath("~"), filePath);
            var serverPath = Server.MapPath("~" + filePath);

            if (!System.IO.File.Exists(serverPath))
                return Json(new { success = false, message = "File Not Found" });

            System.IO.File.Delete(serverPath);
            return Json(new { success = true, message = "File Deleted" });

        }


        private string ResolveLocalFileToUrl(string relativePath, string fileName)
        {
            var url = Request.Url.GetLeftPart(UriPartial.Authority) + HttpRuntime.AppDomainAppVirtualPath + relativePath + "/" + fileName;

            if (!System.IO.File.Exists(Server.MapPath("~") + relativePath + "/" + fileName))
            {
                System.Diagnostics.Debug.WriteLine("File exists: " + url);
                return url;
            }

            return "";
        }
    }
}