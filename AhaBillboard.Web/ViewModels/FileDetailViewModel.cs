﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdvanceLane.ViewModels
{
    public class FileDetailViewModel
    {
        public string name { get; set; }
        public string url { get; set; }
        public string path { get; set; }
    }
}