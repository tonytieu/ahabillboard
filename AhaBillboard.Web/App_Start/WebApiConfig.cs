﻿using System;
using System.Net.Http.Headers;
using System.Web.Http;

public class WebApiConfig
{
    public static void Register(HttpConfiguration configuration)
    {
        configuration.Routes.MapHttpRoute("API Default", "api/{controller}/{id}",
          new { id = RouteParameter.Optional });

        GlobalConfiguration.Configuration.Formatters.JsonFormatter.MediaTypeMappings
 .Add(new System.Net.Http.Formatting.RequestHeaderMapping("Accept",
                               "text/html",
                               StringComparison.InvariantCultureIgnoreCase,
                               true,
                               "application/json"));
    }
}