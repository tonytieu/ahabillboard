namespace AhaBillboard.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addFoodImage : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FoodImages",
                c => new
                    {
                        FoodImageId = c.Guid(nullable: false),
                        Url = c.String(),
                        Food_FoodId = c.Guid(),
                    })
                .PrimaryKey(t => t.FoodImageId)
                .ForeignKey("dbo.Foods", t => t.Food_FoodId)
                .Index(t => t.Food_FoodId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.FoodImages", "Food_FoodId", "dbo.Foods");
            DropIndex("dbo.FoodImages", new[] { "Food_FoodId" });
            DropTable("dbo.FoodImages");
        }
    }
}
