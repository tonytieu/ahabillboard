﻿using Android.Content;
using System.IO;
using System.Xml.Linq;

public class ConfigManager
{
    Context _context;

    public ConfigManager(Context context)
    {
        this._context = context;
    }

    public string GetConfigKey(string configKey)
    {
        string filename = "config.xml";

        using (var reader = new StreamReader(_context.Assets.Open(filename)))
        {
            var doc = XDocument.Parse(reader.ReadToEnd());

            return doc.Element("config").Element(configKey).Value;
        }
    }
}