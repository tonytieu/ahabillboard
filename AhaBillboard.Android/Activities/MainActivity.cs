﻿using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Support.V4.View;
using Android.Support.V4.App;

using FragmentManager = Android.Support.V4.App.FragmentManager;
using Fragment = Android.Support.V4.App.Fragment;
using Environment = Android.OS.Environment;
using System.Net;
using Android.Graphics;

using AhhaBllboard.Core.Service;
using AhhaBllboard.Core;
using AhhaBllboard.Core.Model;
using Android.Util;
using UrlImageViewHelper;
using System.Threading;
using System.IO;
using System.Xml.Linq;
using Android.Content.Res;

namespace AhaBillboard.Android.Activities
{
    [Activity(Label = "AhaBillboard.Android", Icon = "@mipmap/icon", Theme = "@style/AppTheme.NoActionBar")]
    public class MainActivity : FragmentActivity
    {
        private ViewPager _imagePager;
        private ConfigManager _configManager;

        public MainActivity()
        {
            _configManager = new ConfigManager(this);
        }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            _imagePager = FindViewById<ViewPager>(Resource.Id.imagePager);
            _imagePager.Adapter = new ImageFragementAdapter(SupportFragmentManager);

            Direction direction = Direction.ASC;
            TimerExampleState s = new TimerExampleState();
            TimerCallback timerDelegate = new TimerCallback(state => {
                int current = _imagePager.CurrentItem;
                if(current == 0)
                {
                    direction = Direction.ASC;
                }
                if(current == _imagePager.Adapter.Count - 1)
                {
                    direction = Direction.DSC;
                }
                if (direction == Direction.ASC)
                    current++;
                if (direction == Direction.DSC)
                    current--;

                RunOnUiThread(() => {
                    _imagePager.SetCurrentItem(current, true);
                });
                //Application.SynchronizationContext.Post(_ => {
                //    _imagePager.SetCurrentItem(current, true);
                //}, null);
                
            });

            // Create a timer that waits one second, then invokes every 3 seconds.
            Timer timer = new Timer(timerDelegate, s, 3000, 3000);

            // Keep a handle to the timer, so it can be disposed.
            s.tmr = timer;

            // Get our button from the layout resource,
            // and attach an event to it
            //Button button = FindViewById<Button>(Resource.Id.myButton);

            //button.Click += delegate { button.Text = string.Format("{0} clicks!", count++); };

            Window.SetFlags(WindowManagerFlags.Fullscreen, WindowManagerFlags.Fullscreen);

            Window.DecorView.SystemUiVisibility = (StatusBarVisibility)
                                     (SystemUiFlags.LowProfile
                                     | SystemUiFlags.Fullscreen
                                     //| SystemUiFlags.HideNavigation
                                     | SystemUiFlags.Immersive
                                     | SystemUiFlags.ImmersiveSticky);

            Console.WriteLine(_configManager.GetConfigKey("test"));
   
            
        }

        //public string GetKumulosSecretKey()
        //{
        //    return GetBase("kumulos-secret-key");
        //}

        
    }

    enum Direction
    {
        ASC = 0, DSC = 1
    }

    class TimerExampleState
    {
        public int counter = 0;
        public Timer tmr;
    }

    public class ImageFragementAdapter : FragmentPagerAdapter
    {
        private IFoodService _foodService;
        private Food[] _foods;

        //swich to FragemntStatePagerAdapter
        public ImageFragementAdapter(FragmentManager fm)
            : base(fm)
        {
            this._foodService = ServiceContainer.Resolve(typeof(IFoodService)) as IFoodService;
            this._foods = _foodService.GetAll().Result;
        }

        public override Fragment GetItem(int position)
        {
            if(position >= 0 && position < _foods.Length)
                return new ImageFragment(position, this._foods[position]);
            return new ImageFragment();
        }

        public override int Count
        {
            get { return _foods.Length; }
        }

    }

    public class ImageFragment : Fragment
    {
        private ImageView _imageView;
        private TextView tv;
        private int _position;
        private Food _food;
        private bool isImageDownloaded = false;

        public ImageFragment() { }
        public ImageFragment(int position, Food food)
        {
            _position = position;
            this._food = food;
            RetainInstance = true;
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var view = inflater.Inflate(Resource.Layout.ImageFragment, container, false);
            view.Id = _position;

            if(_food != null) {
                _imageView = view.FindViewById<ImageView>(Resource.Id.imageView);

                var img = view.FindViewById<ImageView>(Resource.Id.imageView);
                img.SetUrlDrawable(_food.VideoUrl, Resource.Drawable.loading1);

                //if (!isImageDownloaded)
                //{
                //    GetImageBitmapFromUrlAsync(_food.VideoUrl);
                //    isImageDownloaded = true;
                //}

                //GetImageBitmapFromBase64(_food.VideoUrl);

                tv = view.FindViewById<TextView>(Resource.Id.textView);
                tv.Text = _food.Name + " - " + _food.Price;

            }

            return view;
        }

        private void GetImageBitmapFromBase64(string base64Str)
        {
            var options = new BitmapFactory.Options
            {
                InJustDecodeBounds = false,
            };
            byte[] decodedString = Base64.Decode(base64Str, Base64Flags.Default);
            using (var dispose = BitmapFactory.DecodeByteArray(decodedString, 0, decodedString.Length, options))
                _imageView.SetImageBitmap(dispose);
        }

        private void GetImageBitmapFromUrlAsync(string url)
        {

            WebClient webClient = new WebClient();
            webClient.DownloadDataCompleted += delegate (object sender, DownloadDataCompletedEventArgs e)
            {
                if (e.Result != null && e.Result.Length > 0)
                {
                    var options = new BitmapFactory.Options
                    {
                        InJustDecodeBounds = false,
                    };
                    // BitmapFactory.DecodeResource() will return a non-null value; dispose of it.
                    using (var dispose = BitmapFactory.DecodeByteArray(e.Result, 0, e.Result.Length, options))
                        _imageView.SetImageBitmap(dispose);
                }
            };
            webClient.DownloadDataAsync(new Uri(url));

        }


    }
}

